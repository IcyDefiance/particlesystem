using System;
using System.Collections.Generic;
using System.Timers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Scenes;
using ParticleSystem;

namespace Scenes
{
    public class MainScene : Scene
    {
        private ContentManager content;
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private Effect effectMain;
        private TimeSpan startTime;
        private CameraControls camMain;

        RasterizerState wireRasterizer, solidRasterizer;

        private Model plane;
        private Emitter emitter;

        //debug
        private SpriteFont lucidaFont;
        private Int32 drawCount = 0, drawFps = 0;
        private Timer secondTimer;

        public MainScene(ContentManager content, GraphicsDeviceManager graphics, SpriteBatch spriteBatch, TimeSpan startTime)
        {
            this.content = content;
            this.graphics = graphics;
            this.spriteBatch = spriteBatch;

            this.startTime = startTime;

            LoadContent();
        }

        public override String Update(GameTime gameTime)
        {
            emitter.Update(gameTime, camMain.Position);

            return null;
        }

        public override void Draw()
        {
            graphics.GraphicsDevice.Clear(ClearOptions.Target | ClearOptions.DepthBuffer, Color.Black, 1.0f, 0);

            //draw plane
            graphics.GraphicsDevice.RasterizerState = wireRasterizer;

            Matrix[] planeTransforms = new Matrix[plane.Bones.Count];
            plane.CopyAbsoluteBoneTransformsTo(planeTransforms);
            foreach (ModelMesh mesh in plane.Meshes)
            {
                foreach (Effect currentEffect in mesh.Effects)
                {
                    currentEffect.CurrentTechnique = currentEffect.Techniques["Textured"];
                    currentEffect.Parameters["xWorld"].SetValue(Matrix.Identity * Matrix.CreateRotationX(MathHelper.ToRadians(-90f)) * planeTransforms[mesh.ParentBone.Index]);
                    currentEffect.Parameters["xView"].SetValue(camMain.ViewMatrix);
                    currentEffect.Parameters["xProjection"].SetValue(camMain.ProjectionMatrix);
                }
                mesh.Draw();
            }

            graphics.GraphicsDevice.RasterizerState = solidRasterizer;
            
            //draw particles
            emitter.Draw(camMain.ViewMatrix, camMain.ProjectionMatrix, camMain.Position, new Vector3(0, 1, 0));

            //draw debug
            spriteBatch.Begin();
            spriteBatch.DrawString(lucidaFont, "Draw FPS: " + drawFps, new Vector2(20, 20), Color.White);
            spriteBatch.End();

            drawCount++;
        }

        //-----

        private void LoadContent()
        {
            effectMain = content.Load<Effect>("effects");
            effectMain.Parameters["xTexture"].SetValue(WhiteTex());

            camMain = new CameraControls(graphics);
            camMain.Move(new Vector3(0, 2, 15), true);
            camMain.LookAt(new Vector3(10, 4, 0));

            wireRasterizer = new RasterizerState();
            wireRasterizer.FillMode = FillMode.WireFrame;
            solidRasterizer = new RasterizerState();
            solidRasterizer.FillMode = FillMode.Solid;

            plane = content.Load<Model>("subdividedPlane100sq");
            foreach (ModelMesh mesh in plane.Meshes)
                foreach (ModelMeshPart meshPart in mesh.MeshParts)
                    meshPart.Effect = effectMain.Clone();

            emitter = new Emitter(graphics, content, "line");
            emitter.SelfRotation *= Matrix.CreateRotationZ((float)-Math.PI / 4);
            emitter.Blend = BlendState.Additive;
            emitter.Emitting = true;
            emitter.Scale = new Vector2(0.3f);
            emitter.EmitPower = new Vector2(4f, 6f);
            emitter.Force = new Vector3(0, -0.75f, 0);
            emitter.Lifespan = 5000;
            emitter.EmitRate = 1000;
            emitter.AlphaMode = AlphaMode.CurveOut;
            emitter.AlphaFactor = 3f;
            emitter.AngleLimit = Math.PI / 4;
            emitter.RotationSpeed = new Vector2(1);
            emitter.Initialize();

            //debug
            lucidaFont = content.Load<SpriteFont>("lucida");
            secondTimer = new Timer(1000);
            secondTimer.Elapsed += new ElapsedEventHandler(SecondTick);
            secondTimer.Start();
        }

        private Texture2D WhiteTex()
        {
            Texture2D whiteTex = new Texture2D(graphics.GraphicsDevice, 1, 1);
            whiteTex.SetData(new Color[] { Color.DarkSlateGray });

            return whiteTex;
        }

        //debug
        private void SecondTick(object source, ElapsedEventArgs e)
        {
            drawFps = drawCount;
            drawCount = 0;
        }
    }
}
