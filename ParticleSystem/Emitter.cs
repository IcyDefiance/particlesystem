using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics.PackedVector;

namespace ParticleSystem
{
    public class Emitter
    {
        private GraphicsDeviceManager graphics;
        private ContentManager content;
        private Effect effect;
        private Random random = new Random();

        private DynamicVertexBuffer vbuf;
        private IndexBuffer ibuf;
        private Texture2D particleTex;
        private Double emitTimer;

        private ParticleVertex[] vertices;
        private Int32[] indices;
        private Int32 addIndex, lastAddIndex, drawStart;
        private Int32 maxParticles
        {
            get { return (Int32)(EmitRate * Lifespan / 1000 * 1.1); }
        }

        public Boolean Emitting { get; set; }
        public Double Lifespan { get; set; }
        public Double AngleLimit { get; set; }
        public Matrix SelfRotation { get; set; }
        public Vector3 Position { get; set; }
        public Vector3 Velocity { get; set; }
        public Vector3 Force { get; set; }
        public Vector2 EmitPower { get; set; }  //min/max
        public Vector2 Scale { get; set; }      //min/max
        public Vector2 RotationSpeed { get; set; }
        public BlendState Blend { get; set; }
        public AlphaMode AlphaMode { get; set; }
        public float AlphaFactor { get; set; }

        private Double emitTick;
        public Double EmitRate
        {
            get { return 1000 / emitTick; }
            set { emitTick = 1000 / value; }
        }

        public Emitter(GraphicsDeviceManager graphics, ContentManager content, Color color)
        {
            this.graphics = graphics;
            this.content = content;
            Construct(graphics, content, ColorTex(color));
        }
        public Emitter(GraphicsDeviceManager graphics, ContentManager content, String texName)
        {
            Construct(graphics, content, content.Load<Texture2D>(texName));
        }

        private void Construct(GraphicsDeviceManager graphics, ContentManager content, Texture2D tex)
        {
            this.graphics = graphics;
            this.content = content;
            Blend = BlendState.Opaque;
            Emitting = false;

            effect = content.Load<Effect>("particleEffects");
            effect.CurrentTechnique = effect.Techniques["Particles"];

            particleTex = tex;

            Scale = new Vector2(1f);
            EmitPower = new Vector2(1);
            Lifespan = 1000;
            EmitRate = 100;
            emitTimer = emitTick;
            AngleLimit = Math.PI * 2;
            SelfRotation = Matrix.Identity;

            AlphaMode = ParticleSystem.AlphaMode.Flat;
            AlphaFactor = 1;

            addIndex = 0;
        }

        public void Initialize()
        {
            vertices = new ParticleVertex[maxParticles * 4];
            indices = new Int32[maxParticles * 6];

            for (Int32 i = 0; i < vertices.Length; i += 4)
            {
                vertices[i + 0].Corner = new Short2(0, 0);
                vertices[i + 1].Corner = new Short2(1, 0);
                vertices[i + 2].Corner = new Short2(1, 1);
                vertices[i + 3].Corner = new Short2(0, 1);
                vertices[i + 0].Time = 0;
                vertices[i + 1].Time = 0;
                vertices[i + 2].Time = 0;
                vertices[i + 3].Time = 0;

                indices[(Int32)(i * 1.5 + 0)] = i + 0;
                indices[(Int32)(i * 1.5 + 1)] = i + 1;
                indices[(Int32)(i * 1.5 + 2)] = i + 2;
                indices[(Int32)(i * 1.5 + 3)] = i + 2;
                indices[(Int32)(i * 1.5 + 4)] = i + 3;
                indices[(Int32)(i * 1.5 + 5)] = i + 0;
            }

            vbuf = new DynamicVertexBuffer(graphics.GraphicsDevice, ParticleVertex.VertexDeclaration, vertices.Length, BufferUsage.WriteOnly);
            ibuf = new IndexBuffer(graphics.GraphicsDevice, typeof(Int32), indices.Length, BufferUsage.WriteOnly);
            ibuf.SetData(indices);
        }

        public void Update(GameTime gameTime, Vector3 camPos)
        {
            //update emitter
            Position += Vector3.Multiply(Velocity, (float)gameTime.ElapsedGameTime.TotalSeconds);

            //add new particles
            lastAddIndex = addIndex;
            if (Emitting)
            {
                emitTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
                while (emitTimer < 0)
                {
                    EmitParticle(gameTime.TotalGameTime.TotalMilliseconds);
                    emitTimer += emitTick;
                }
            }

            //prep for drawing
            drawStart = FindDrawStart(gameTime.TotalGameTime.TotalMilliseconds);
            AddToBuffers();
            effect.Parameters["xTime"].SetValue((float)gameTime.TotalGameTime.TotalMilliseconds);
        }

        public void Draw(Matrix viewMatrix, Matrix projMatrix, Vector3 camPos, Vector3 camUp)
        {
            //if (particles == null || particles.Count < 1) return;

            effect.Parameters["xView"].SetValue(viewMatrix);
            effect.Parameters["xProjection"].SetValue(projMatrix);
            effect.Parameters["xCamPos"].SetValue(camPos);
            effect.Parameters["xCamUp"].SetValue(camUp);
            effect.Parameters["xTexture"].SetValue(particleTex);
            effect.Parameters["xPointSpriteSize"].SetValue(Scale.Y);
            effect.Parameters["xRotationSpeed"].SetValue(RotationSpeed);
            effect.Parameters["xForce"].SetValue(Force);
            effect.Parameters["xLifespan"].SetValue((float)Lifespan);
            effect.Parameters["xAlphaMode"].SetValue((int)AlphaMode);
            effect.Parameters["xAlphaFactor"].SetValue(AlphaFactor);

            graphics.GraphicsDevice.BlendState = Blend;
            graphics.GraphicsDevice.Indices = ibuf;
            graphics.GraphicsDevice.SetVertexBuffer(vbuf);

            
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();

                if (drawStart < addIndex)
                    graphics.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, drawStart, addIndex - drawStart, (Int32)(drawStart * 1.5), (addIndex - drawStart) / 2);
                else
                {
                    graphics.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, drawStart, vertices.Length - drawStart, (Int32)(drawStart * 1.5), (vertices.Length - drawStart) / 2);
                    if (addIndex > 0)
                        graphics.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, addIndex, 0, addIndex / 2);
                }
            }

            graphics.GraphicsDevice.BlendState = BlendState.Opaque;
        }

        private void EmitParticle(Double timeInMillisecs)
        {
            Vector3 initialVelocity = new Vector3();
            initialVelocity.Y = random.Next() * ((EmitPower.Y - EmitPower.X) / (int.MaxValue - 1)) + EmitPower.X;
            Matrix rotMatrix = Matrix.CreateRotationX((float)((random.NextDouble() * AngleLimit) - (AngleLimit / 2))) * Matrix.CreateRotationY((float)(random.NextDouble() * Math.PI * 2)) * SelfRotation;
            initialVelocity = Vector3.Transform(initialVelocity, rotMatrix);

            initialVelocity += Velocity;

            vertices[addIndex + 0].Position = vertices[addIndex + 1].Position = vertices[addIndex + 2].Position = vertices[addIndex + 3].Position = Position;
            vertices[addIndex + 0].Velocity = vertices[addIndex + 1].Velocity = vertices[addIndex + 2].Velocity = vertices[addIndex + 3].Velocity = initialVelocity;
            vertices[addIndex + 0].Time = vertices[addIndex + 1].Time = vertices[addIndex + 2].Time = vertices[addIndex + 3].Time = (float)timeInMillisecs;
            vertices[addIndex + 0].Random.R = vertices[addIndex + 1].Random.R = vertices[addIndex + 2].Random.R = vertices[addIndex + 3].Random.R = (byte)random.Next((int)(Scale.X / Scale.Y * 255), 256);
            vertices[addIndex + 0].Random.G = vertices[addIndex + 1].Random.G = vertices[addIndex + 2].Random.G = vertices[addIndex + 3].Random.G = (byte)random.Next(256);

            addIndex += 4; if (addIndex == vertices.Length) addIndex = 0;
        }

        private Int32 FindDrawStart(Double timeInMillisecs)
        {
            if (timeInMillisecs < Lifespan) return 0;

            for (int i = drawStart; i < vertices.Length; i += 4)
                if (timeInMillisecs - vertices[i].Time < Lifespan) return i;
            for (int i = 0; i < vertices.Length; i += 4)
                if (timeInMillisecs - vertices[i].Time < Lifespan) return i;

            return -1;
        }

        private void AddToBuffers()
        {
            if (drawStart == addIndex) return;
            if (vbuf.IsContentLost)
            {
                vbuf.SetData(vertices);
                return;
            }

            int stride = ParticleVertex.SizeInBytes;
            if (lastAddIndex < addIndex)
                vbuf.SetData(lastAddIndex * stride, vertices, lastAddIndex, (addIndex - lastAddIndex), stride, SetDataOptions.NoOverwrite);
            else
            {
                vbuf.SetData(lastAddIndex * stride, vertices, lastAddIndex, vertices.Length - lastAddIndex, stride, SetDataOptions.NoOverwrite);
                if (addIndex > 0)
                    vbuf.SetData(0, vertices, 0, addIndex, stride, SetDataOptions.NoOverwrite);
            }
        }

        private Texture2D ColorTex(Color color)
        {
            Texture2D whiteTex = new Texture2D(graphics.GraphicsDevice, 1, 1);
            whiteTex.SetData(new Color[] { color });

            return whiteTex;
        }
    }
}
