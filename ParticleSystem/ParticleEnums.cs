﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParticleSystem
{
    public enum AlphaMode
    {
        Flat, CurveOut, CurveInOut
    }
}
