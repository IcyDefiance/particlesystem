//------------------------------------------------------
//--                                                  --
//--		   www.riemers.net                    --
//--   		    Basic shaders                     --
//--		Use/modify as you like                --
//--                                                  --
//------------------------------------------------------

struct VertexShaderInput
{
    float2 Corner : POSITION0;
    float3 Position : POSITION1;
    float3 Velocity : NORMAL0;
    float4 Random : COLOR0;
    float Time : TEXCOORD0;
};

struct VertexToPixel
{
    float4 Position   	: POSITION;    
    float4 Color		: COLOR0;
    float LightingFactor: TEXCOORD0;
    float2 TextureCoords: TEXCOORD1;
};

struct PixelToFrame
{
    float4 Color : COLOR0;
};

//------- Constants --------
float4x4 xView;
float4x4 xProjection;
float3 xCamPos;
float3 xCamUp;
float3 xForce;
float2 xRotationSpeed;
float xPointSpriteSize;
float xTime;
float xLifespan;
float xAlphaFactor;
uint xAlphaMode;

//------- Texture Samplers --------

Texture xTexture;
sampler TextureSampler = sampler_state { texture = <xTexture>; magfilter = LINEAR; minfilter = LINEAR; mipfilter=LINEAR; AddressU = mirror; AddressV = mirror;};

//------- Technique: Particles --------

VertexToPixel ParticlesVS(VertexShaderInput input)
{
    VertexToPixel Output = (VertexToPixel)0;
	float elapsedTime = (xTime - input.Time) / 1000;
	
	//rotation
	float rotation = lerp(xRotationSpeed.x, xRotationSpeed.y, input.Random.y) * elapsedTime;
    float c = cos(rotation);
    float s = sin(rotation);
	float2 corner = mul(float2(input.Corner.x-0.5f, 0.5f-input.Corner.y), float2x2(c, -s, s, c));

	//position
    float3 center = input.Position + (input.Velocity * elapsedTime) + (xForce * elapsedTime * elapsedTime);
    float3 eyeVector = center - xCamPos;
    float3 sideVector = normalize(cross(eyeVector,xCamUp));
    float3 upVector = normalize(cross(sideVector,eyeVector));

    float3 finalPosition = center;
    finalPosition += corner.x*sideVector*0.5f*xPointSpriteSize*input.Random.x;
    finalPosition += corner.y*upVector*0.5f*xPointSpriteSize*input.Random.x;

    float4 finalPosition4 = float4(finalPosition, 1);
	
    float4x4 preViewProjection = mul(xView, xProjection);
    Output.Position = mul(finalPosition4, preViewProjection);

	//texture
    Output.TextureCoords = input.Corner;

	//alpha curve
    float normAge = saturate(elapsedTime * 1000 / xLifespan);
	Output.Color = float4(1, 1, 1, 1);
    [flatten] switch (xAlphaMode)
    {
	case 1:     //CurveOut
        Output.Color *= (1 - (normAge * normAge)) * xAlphaFactor;
        break;
    case 2:     //CurveInOut
        Output.Color *= normAge * (1 - normAge) * (1 - normAge) * 6.7 * xAlphaFactor;
        break;
    }

    return Output;
}

PixelToFrame ParticlesPS(VertexToPixel PSIn) : COLOR0
{
    PixelToFrame Output = (PixelToFrame)0;
    Output.Color = tex2D(TextureSampler, PSIn.TextureCoords) * PSIn.Color;
    return Output;
}

technique Particles
{
	pass Pass0
	{   
		VertexShader = compile vs_3_0 ParticlesVS();
		PixelShader  = compile ps_3_0 ParticlesPS();
	}
}