using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Scenes
{
    public abstract class Scene
    {
        public abstract String Update(GameTime gameTime);
        public abstract void Draw();
    }
}
