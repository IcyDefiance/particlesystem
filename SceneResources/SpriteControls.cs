﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Scenes
{
    public class SpriteControls
    {
        private GraphicsDeviceManager Graphics;
        private SpriteBatch SpriteBatch;

        private Texture2D texture;
        public Texture2D Texture
        {
            get { return texture; }
            set
            {
                texture = value;

                Color[] colors1D = new Color[Texture.Width * Texture.Height];
                Texture.GetData(colors1D);

                Color[,] colors2D = new Color[Texture.Width, Texture.Height];
                for (int x = 0; x < Texture.Width; x++)
                    for (int y = 0; y < Texture.Height; y++)
                        colors2D[x, y] = colors1D[x + y * Texture.Width];

                TexAsColorArray = colors2D;
            }
        }
        public Color[,] TexAsColorArray { get; set; }
        public float Alpha { get; set; }
        public Vector2 Scale { get; set; }
        public Vector2 Rotation { get; set; }

        public SpriteControls Parent { get; set; }
        public List<SpriteControls> Children { get; set; }

        private Vector2 position;
        public Vector2 Position
        {
            get { return position; }
            set
            {
                Vector2 movement = value - position;
                if (Children != null)
                {
                    foreach (SpriteControls Child in Children)
                    {
                        Child.Position += movement;
                    }
                }
                position = value;
            }
        }

        public Vector2 Size
        {
            get { return new Vector2(Texture.Width * Scale.X, Texture.Height * Scale.Y); }
        }
        public Vector2 SizeBeforeScale
        {
            get { return new Vector2(Texture.Width, Texture.Height); }
        }

        public Vector2 Center
        {
            get { return Vector2.Multiply(Size, 0.5f); }
        }

        public SpriteControls(GraphicsDeviceManager graphics, ContentManager content, SpriteBatch spriteBatch, String assetName)
        {
            SpriteBatch = spriteBatch;
            Graphics = graphics;

            Texture = content.Load<Texture2D>(assetName);

            Position = new Vector2(0f);
            Rotation = new Vector2(0f);
            Scale = new Vector2(1f);
            Alpha = 1f;
            Parent = null;
            Children = new List<SpriteControls>();
        }

        public void Draw()
        {
            SpriteBatch.Draw(Texture, new Rectangle((Int32)Position.X, (Int32)Position.Y, (Int32)(Texture.Width * Scale.X), (Int32)(Texture.Height * Scale.Y)), Color.White * Alpha);
        }

        public void PositionCenter() { PositionCenter(new Vector2(0f)); }
        public void PositionCenter(Vector2 offset)
        {
            Vector2 screenCenter = new Vector2(Graphics.PreferredBackBufferWidth, Graphics.PreferredBackBufferHeight) * 0.5f;
            Vector2 imageCenter = new Vector2(Texture.Width * Scale.X, Texture.Height * Scale.Y) * 0.5f;
            Position = screenCenter - imageCenter + offset;
        }

        public Boolean IsMouseOver()
        {
            MouseState mouse = Mouse.GetState();
            if (mouse.X >= Position.X && mouse.Y >= Position.Y && mouse.X <= Position.X + Texture.Width && mouse.Y <= Position.Y + Texture.Height)
                return true;
            else
                return false;
        }

        public void AddParent(SpriteControls sprite)
        {
            sprite.Children.Add(this);
            Parent = sprite;
        }

        public void RemoveParent()
        {
            Parent.Children.Remove(this);
            Parent = null;
        }

        public void AddChild(SpriteControls sprite)
        {
            sprite.Parent = this;
            Children.Add(sprite);
        }

        public void RemoveChild(SpriteControls sprite)
        {
            sprite.Parent = null;
            Children.Remove(sprite);
        }
    }
}
