﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Scenes
{
    public class CameraControls
    {
        private Matrix viewMatrix;
        public Matrix ViewMatrix
        {
            get { return viewMatrix; }
        }

        private Matrix projMatrix;
        public Matrix ProjectionMatrix
        {
            get { return projMatrix; }
        }

        private GraphicsDeviceManager graphics;
        private Vector3 position, lookAt, up;
        public Vector3 Position
        {
            get { return position; }
        }

        /// <summary>
        /// Creates a new camera at 0,0,0, looking forward.
        /// </summary>
        /// <param name="graphics">Graphics device manager</param>
        /// <param name="position">Starting position</param>
        /// <param name="lookAt">Starting point to look at</param>
        public CameraControls(GraphicsDeviceManager graphicsIn, Vector3? positionIn = null, Vector3? lookAtIn = null)
        {
            position = positionIn ?? new Vector3(0, 0, 0);
            lookAt = lookAtIn ?? new Vector3(0, 0, -1);
            up = new Vector3(0, 1, 0);

            Initialize(graphicsIn, position, lookAt);
        }

        /// <summary>
        /// Moves the camera to a position relative to its current position
        /// </summary>
        /// <param name="moveVector">Direction and distance</param>
        /// <param name="keepLookAt">Turn the camera too keep looking at the same point, defaults to false.</param>
        public void Move(Vector3 moveVector, Boolean keepLookAt = true)
        {
            position = Vector3.Add(position, moveVector);
            if (keepLookAt) lookAt = Vector3.Add(lookAt, moveVector);

            viewMatrix = Matrix.CreateLookAt(position, lookAt, up);
        }

        public void LookAt(Vector3 lookAt, Boolean keepRotation = false)
        {
            if (keepRotation) position += Vector3.Subtract(this.lookAt, lookAt);
            this.lookAt = lookAt;

            viewMatrix = Matrix.CreateLookAt(position, lookAt, up);
        }

        //-----

        private void Initialize(GraphicsDeviceManager graphicsIn, Vector3 position, Vector3 lookAt)
        {
            graphics = graphicsIn;

            viewMatrix = Matrix.CreateLookAt(position, lookAt, up);
            projMatrix = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, graphics.GraphicsDevice.Viewport.AspectRatio, 1.0f, 500.0f);
        }
    }
}
